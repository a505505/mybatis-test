package com.it.mybatis.mapper;

import com.it.mybatis.bean.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {

    User getUser(Integer id);

    List<User> getUserByName(@Param("name") String name);

    Integer inserUser(User user);

    @Select("select * from user where id = #{id}")
    User findUserList(@Param("id") long id, @Param("name") String name);
}
